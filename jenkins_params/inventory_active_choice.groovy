import groovy.json.JsonSlurper

def out = new ByteArrayOutputStream()
def err = new ByteArrayOutputStream()

def token = "vOY1kEFkHxHwIT6IaQFI73gnRxpaNi"
def url = "https://10.85.206.26/api/v2/inventories/"
def cmd = ["bash", "-c", "curl --noproxy '*' -k -s ${url} --header 'Authorization: Bearer ${token}' --header 'Content-Type: application/json'"]

def proc = cmd.execute()

proc.consumeProcessOutput(out, err)
proc.waitForOrKill(1000)

def jsonSlurper = new JsonSlurper()
def invs = jsonSlurper.parseText(out.toString())

def list = []

invs.results.each {
  def key = it.id
  def item = [(key): it.name]
  list.add(item)
}

return list
