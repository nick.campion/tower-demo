# Jenkins Trigger Ansible Tower Job

## Pre Req's

- Create an application within Ansible Tower to allow you to bind the access token.

![Create Application](img/application.png)

- Create a Token for the API user so that Jenkins can trigger the jobs

The token will require write scope in order to create new jobs.

![Create User Token](img/create-token.png)

Grab the Token for use in the Jenkins pipeline

![Copy Token](img/copy-token.png)

- Get the Template ID that you want Jenkins to run
This can be found in either the URL for the job template, or by querying the API

```bash
curl 'https://tower.mgmt.vmer.dwpcloud.uk/api/v2/job_templates/' \
-H 'Authorization: Bearer vOY1kEFkHxHwIT6IaQFI73gnRxpaNi' \
-H 'Content-Type: application/json'
```

## Set up the Jenkins Job

_[A working example can be found here](http://jenkins.mgmt.vmer.dwpcloud.uk/job/development/job/package-test/)_

- Create a new pipeline (ensure the name has no white spaces)

- Enable Parameters

- Add an Active Choice Parameter called `pkg`. This needs to return a list of packages from s3. An example Groovy script can be found [here](https://gitlab.com/nick.campion/tower-demo/-/blob/main/jenkins_params/pkg_active_choice.groovy)

- Add an Active Choice Parameter called `inventory`. This needs to pull in a list of inventories from the Tower instance. An example is [here](https://gitlab.com/nick.campion/tower-demo/-/blob/main/jenkins_params/inventory_active_choice.groovy) - you will have to update the Tower Token in order for this to work. 

- Add a Parameter for `tower_host` that reflects a URL the Jenkins slaves can reach. ([See previous example for reference](enkins.mgmt.vmer.dwpcloud.uk/job/development/job/package-test/configure))

- Add a Paramerter for `job` that reflects the tower Template ID to run.

- Add a Parameter for the Tower Access Token to be passed to the Pipeline.

- Create your pipeline to execute [this script](https://gitlab.com/nick.campion/tower-demo/-/blob/main/scripts/start_job.sh)

```bash
# Example of running the script. Because both PKG & INVENTORY are arrays, these need to be quoted.

./start_job.sh -t ${TOKEN} -p \"${PKG}\" -i \"${INVENTORY}\" -h ${TOWER_HOST} -j ${JOB}

```

An example Jenkinsfile is available [in this repo](https://gitlab.com/nick.campion/tower-demo/-/blob/main/jenkinsfile) 

